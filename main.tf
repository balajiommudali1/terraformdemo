resource "ibm_is_vpc" "testacc_vpc_new" {
  name = "test-vpc-new"
}

output "testacc_vpc" {
  value = ibm_is_vpc.testacc_vpc_new.id
}
